import Tabledetails from "./Tabledetails";
import styles from "./Details.module.scss";
import {useEffect, useState} from "react";
import axios from "axios";
const Details = () => {
  const [detailstabledata, setDetailsTableData] = useState([]);
  const token = localStorage.getItem("token");
  useEffect(() => {
    const getDetailsData = async () => {
      try {
        await axios
          .get("/students", {
            headers: {
              Authorization: token,
            },
          })
          .then((response) => {
            console.log(response);
            const detailsdata = response.data.message;
            setDetailsTableData(detailsdata);
          });
      } catch (error) {
        console.log(error);
      }
    };
    getDetailsData();
  }, []);
  console.log(detailstabledata);
  return (
    <div>
      <div className={styles.heading}>
        <h1>Activity Details Page</h1>
      </div>
      <div className={styles.heading}>
        <h3>Company Name| Domain | NoOfStudents</h3>
      </div>
      <div className={styles.tabledata}>
        <Tabledetails
          rows={detailstabledata}
          // rows={[
          //   {
          //     id: 1,
          //     name: "shivani",
          //     year: "final",
          //     average: 80,
          //     cv: "sinya",
          //     Details: "abc",
          //   },
          // ]}
        />
      </div>
    </div>
  );
};
export default Details;
