import React from "react";
import PropTypes from "prop-types";
//import {withStyles} from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import styles from "./Details.module.scss";
import {useHistory} from "react-router-dom";
//import {Props} from "./Tabledata.types";
//import Addnewactivity from "../../page/Addnewactivity/Addnewactivity";
//import styles from "./Tabledata.module.scss";
import Updatestudent from "../Addnewactivity/Updatestudent";
//import styles from "./Details.module.scss";
interface Props {
  rows: Array<{
    id: number;
    name: string;
    year: string;
    average: number;
    cv: string;
    Details: string;
    Edit:string;
  }>;
}

// const changeHandler = (e:any) => {
//   setCv(e.target.files[0]);
//   setIsFilePicked(true);
//   console.log(cv,"cv");
// };
const Tabledetails = ({rows}: Props) => {
  let history = useHistory();
  // const UpdateStudent=()=>{
  //   history.push("/Updatestudent");
  // }
  return (
    <div>
      <Paper>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Id</TableCell>
              <TableCell>Student</TableCell>
              <TableCell>Year</TableCell>
              <TableCell>Avg%</TableCell>
              <TableCell>CV</TableCell>
              <TableCell>Action</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <TableRow key={row.id}>
                <TableCell>{row.id}</TableCell>
                <TableCell>{row.name}</TableCell>
                <TableCell>{row.year}</TableCell>
                <TableCell>{row.average}</TableCell>
                <TableCell>{row.cv}</TableCell>
                <TableCell>
                  <div className={styles.rowsetdiv}>
                  <button className={styles.detailsbtn} onClick={()=>history.push(`/Updatestudent/${row.id}`)} >Edit</button>
                  <button className={styles.detailsbtn}>Details</button>
                  </div>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Paper>
    </div>
  );
};
export default Tabledetails;

//export default Tabledata;
///onClick={UpdateStudent}
