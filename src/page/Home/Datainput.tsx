import {useState} from "react";
import {Iprops} from "./Datainput.types";
//import {nanoid} from "nanoid";
import {Button, tabClasses, TextField} from "@material-ui/core";
import axios from "axios";
import styles from "./Datainput.module.scss";
const Datainput = ({inputdata, setInputData, setBtnClick}: any) => {
  const token = localStorage.getItem("token");
  // const [inputdata, setInputData] = useState({
  //   company: "",
  //   date: "",
  //   domain: "",
  //   vacancy: "",
  //   student: [],
  // });
  // const handleChange = (e: any) => {
  //   e.preventDefault();
  //   const fieldName = e.target.getAttribute("name");
  //   const fieldvalue = e.target.value;
  //   const {...newFormdata} = inputdata;
  //   newFormdata[fieldName] = fieldvalue;
  //   console.log("newformdatastring", newFormdata);
  //   setInputData(newFormdata);
  // };
  // console.log(inputdata);
  const handleFormSubmit = async (e: any) => {
    e.preventDefault();
    setBtnClick("butnclicked");
    const newdata = {
      company: inputdata.company,
      date: inputdata.date,
      domain: inputdata.domain,
      vacancy: inputdata.vacancy,
      students: [],
    };
    //await axios post("url",newdata)
    // const setNewData = async () => {
    axios
      .post(
        "/activity/create",
        {data: newdata},
        {
          headers: {
            Authorization: token,
          },
        }
      )
      .then(
        (response) => {
          console.log(response);
        },
        (error) => {
          console.log(error);
        }
      );
    console.log("stringnewdata", newdata);
    // };
  };

  return (
    <div>
      <div>
      <h1>Today's Campus Activity</h1>
      </div>
      <form onSubmit={handleFormSubmit}>
      <TextField id="outlined-basic" label="enter company name" variant="outlined" name="company" onChange={(e) => {
            setInputData({...inputdata, company: e.target.value});
          }}/>
      <TextField id="outlined-basic" label="enter date" variant="outlined" name="date" onChange={(e) => {
            setInputData({...inputdata, date: e.target.value});
          }}/>
      <TextField id="outlined-basic" label="enter domain" variant="outlined" name="domain" onChange={(e) => {
            setInputData({...inputdata, domain: e.target.value});
          }}/>
      <TextField id="outlined-basic" label="enter vacancy" variant="outlined" name="vacancy" onChange={(e) => {
            setInputData({...inputdata, vacancy: e.target.value});
          }}/>
        {/* <input
          type="text"
          placeholder="enter company name"
          name="company"
          onChange={(e) => {
            setInputData({...inputdata, company: e.target.value});
          }}
        /> */}

        {/* <input
          type="text"
          placeholder="enter date"
          name="date"
          onChange={(e) => {
            setInputData({...inputdata, date: e.target.value});
          }}
        /> */}
        {/* <input
          type="text"
          placeholder="enter domain"
          name="domain"
          onChange={(e) => {
            setInputData({...inputdata, domain: e.target.value});
          }}
        />
        <input
          type="number"
          placeholder="enter vacancy"
          name="vacancy"
          onChange={(e) => {
            setInputData({...inputdata, vacancy: e.target.value});
          }}
        /> */}
        <button type="submit" className={styles.addbtn}>Add Student</button>
        {/* <Button variant="contained" component="span"  type="submit">
          Add Company
        </Button>   */}
      </form>
    </div>
  );
};
export default Datainput;
