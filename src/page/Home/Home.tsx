import Tabledata from "../../component/Table/Tabledata";
import axios from "axios";
import {useEffect, useState} from "react";
import {ResetTvRounded} from "@mui/icons-material";
import Datainput from "./Datainput";
import { useParams } from "react-router-dom";

interface Iparam {
  companyId: string;
}
// interface Props {
//   rows: Array<{
//     id: number;
//     Company: string;
//     Domain: string;
//     numberOfStudents: number;
//     Action: string;
//   }>;
// }
// const newFormdata: {
//   companyname: string;
//   domain: string;
//   vacancy: string;
// }
const Home = () => {
  const [inputdata, setInputData] = useState({
    company: "",
    date: "",
    domain: "",
    vacancy: "",
    student: [],
  });
  const { companyId } = useParams<Iparam>();
  const [tabledata, setTableData] = useState([]);
  const [btnclick, setBtnClick] = useState("");
  const token = localStorage.getItem("token");
  console.log(token);
  useEffect(() => {
    const getData = async () => {
      try {
        await axios
          .get("/activity", {
            headers: {
              Authorization: token,
            },
          })
          .then((response) => {
            console.log(response);
            const tdata = response.data.message;
            setTableData(tdata);
          });
      } catch (error) {
        console.log(error);
      }
    };
    getData();
  }, [btnclick]);
  console.log(tabledata);

  //   await axios.get("/company",{ headers : {
  //     "Authorization":token
  //     }
  // })

  return (
    <div>
      <Datainput
        inputdata={inputdata}
        setInputData={setInputData}
        setBtnClick={setBtnClick}
      />
      <Tabledata rows={tabledata} />
    </div>
  );
};
export default Home;

// const Home = () => {
//   const [tabledata, setTableData] = useState([]);
//   const token = localStorage.getItem("token");
//   console.log(token);
//   useEffect(() => {
//     const getData = async () => {
//       try {
//         await axios
//           .get("/activity", {
//             headers: {
//               Authorization: token,
//             },
//           })
//           .then((response) => {
//             console.log(response);
//             const tdata = response.data.message;
//             setTableData(tdata);
//           });
//       } catch (error) {
//         console.log(error);
//       }
//     };
//     getData();
//   }, []);
//   console.log(tabledata);

//   //   await axios.get("/company",{ headers : {
//   //     "Authorization":token
//   //     }
//   // })

//   return (
//     <div>
//       <form>
//         <input type="text" placeholder="enter company name"/>
//       </form>
//       <Tabledata rows={tabledata} />
//     </div>
//   );
// };
// export default Home;

// rows={[
//   {
//     id: 1,
//     Company: "Coditas",
//     Domain: "React",
//     numberOfStudents: 10,
//     Action: "newpage",
//   },
// ]}
// {
//   /* <Tabledata
// rows={[
//   {
//     id: 1,
//     Company: "Coditas",
//     Domain: "React",
//     numberOfStudents: 10,
//     Action: "newpage",
//   },
// ]}
// /> */
// }
// import {useForm} from "react-hook-form";
// import {yupResolver} from "@hookform/resolvers/yup";
// import * as yup from "yup";
// import {IForm} from "./Login.types";
// import {useState} from "react";
// import {Link, useHistory} from "react-router-dom";
// import Home from "../../page/Home/Home";
// import Validate from "./Validate";
// import axios from "axios";

// const loginValid = yup.object().shape({
//   // userName: yup.string().email().required(),
// });
// type userSubmitForm = {
//   username: string;
//   password: string;
// };

// const schema = yup.object().shape({
//   username: yup
//     .string()
//     .required("Please Enter your email")
//     .matches(/^[^\s@]+@[^\s@]+\.[^\s@]+$/),

//   password: yup
//     .string()
//     .required("Please Enter your password")
//     .matches(
//       /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
//       "Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and one special case Character"
//     ),
// });

// const Login = () => {
//   const history = useHistory();
//   const [error, setError] = useState(false);

//   // const {
//   //   register,
//   //   handleSubmit,
//   //   formState: {errors},
//   // } = useForm<IForm>({
//   //   resolver: yupResolver(schema),
//   // });
//   const {
//     register,
//     handleSubmit,

//     formState: {errors},
//   } = useForm({
//     resolver: yupResolver(loginValid),
//   });

//   // const onSubmit = (data: IForm) => {
//   //   const result = true;
//   //   if (result) {
//   //     setError(false);
//   //     history.push("/Home");
//   //   } else {
//   //     setError(true);
//   //   }
//   // };

//   // const handleclick = () => {
//   //   console.log("formdata submited");
//   // };
//   const onSubmit = async (data: userSubmitForm) => {
//     // console.log(data)
//     axios

//       .post("/user/login", {
//         username: data.username,
//         password: data.password,
//       })
//       .then((response) => {
//         console.log(response);
//         if (response.data.token) {
//           setToken(response.data.token);
//           alert("login successfull");
//           history.push("/activity");
//           // console.log("if");
//           // console.log(response.data.message);
//         } else {
//           // console.log("else");
//           alert("login failed");

//           // console.log(response);
//         }
//       });
//   };

//   return (
//     <>
//       <div className={styles.container} onClick={handleclick}>
//         <div className={styles.maincontent}>
//           <form onSubmit={handleSubmit(onSubmit)}>
//             <h2>Login Here</h2>
//             <input
//               type="email"
//               placeholder="email"
//               className={styles.textFiled}
//               {...register("username")}
//             />
//             <p> {errors.username?.message}</p>
//             <input
//               type="password"
//               placeholder="password"
//               className={styles.textFiled}
//               {...register("password")}
//             />
//             {errors.password && <p>{errors.password.message}</p>}
//             <div>
//               <button className={styles.btn}>Login</button>
//             </div>

//             {/* <Link to="/Home">Forgot Password?</Link> */}
//           </form>
//         </div>
//       </div>
//     </>
//   );
// };

// export default Login;
// function setToken(token: any) {
//   throw new Error("Function not implemented.");
