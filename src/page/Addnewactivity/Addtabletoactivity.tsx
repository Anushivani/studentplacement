import React from "react";
import PropTypes from "prop-types";
//import {withStyles} from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import styles from "./Details.module.scss";
import {useHistory} from "react-router-dom";
import {Link} from 'react-router-dom';
import Updatestudent from "./Updatestudent";
//import {Props} from "./Tabledata.types";
//import Addnewactivity from "../../page/Addnewactivity/Addnewactivity";
//import styles from "./Tabledata.module.scss";

interface Props {
  rows: Array<{
    id: number;
    name: string;
    year: string;
    average: number;
    cv: string;
    Details: string;
    Edit:string;
  }>;
}
let history = useHistory();
const UpdateStudent=()=>{
   console.log("studentupdated");
  //window.open("/Updatestudent");
  history.push("/Updatestudent");
}
// handleClick(){
//   window.open("/insert/your/path/here");
// }
const editData=()=>{
  alert("this is update ")
  history.push("/Updatestudent")
}
const Addtabletoactivity = ({rows}: Props) => {
  return (
    <div>
      <Paper>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Id</TableCell>
              <TableCell>StudentName</TableCell>
              <TableCell>Year</TableCell>
              <TableCell>Avg%</TableCell>
              <TableCell>CV</TableCell>
              <TableCell>Action</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <TableRow key={row.id}>
                <TableCell>{row.id}</TableCell>
                <TableCell>{row.name}</TableCell>
                <TableCell>{row.year}</TableCell>
                <TableCell>{row.average}</TableCell>
                <TableCell>{row.cv}</TableCell>
                <TableCell>
               
                  {/* <Link to="/Updatestudent" className={styles.detailsbtn}>Edit</Link> */}
                  <button className={styles.detailsbtn}>Edit</button>
                  <button className={styles.detailsbtn} onClick={editData}>Details</button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Paper>
    </div>
  );
};
export default Addtabletoactivity;
