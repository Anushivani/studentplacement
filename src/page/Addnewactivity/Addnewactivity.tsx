import * as React from "react";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import styles from "./Addnewactivity.module.scss";
//import Tabledetails from "../Details/Details";
//import Tabledata from "../../component/Table/Tabledata";
import Addtabletoactivity from "./Addnewactivity";
import Tabledetails from "./../Details/Tabledetails";
import axios from "axios";
import {useState, useEffect} from "react";
import { setTokenSourceMapRange } from "typescript";
import { styled } from '@mui/material/styles';
import Button from '@mui/material/Button';
// interface Props {
//   rows: Array<{
//     id: string;
//     Student: string;
//     Year: string;
//     numberOfStudents: string;
//     Avg: string;
//     CV: any;
//     Details: string;
//   }>;
// }
interface Props {
  rows: Array<{
    id: number;
    name: string;
    year: string;
    average: number;
    cv: string;
    Details: string;
    Edit:any;
  }>;
}
const Input = styled('input')({
  display: 'none',
});
const Addnewactivity = () => {
  const [name,setName]=useState("");
  const [year,setYear]=useState("");
  const[average,setAverage]=useState("");
  const[cv,setCv]=useState();
  const [isFilePicked, setIsFilePicked] = useState(false);
  const [detailstabledata, setDetailsTableData] = useState([]);
  const[updatestudent,setUpdateStudent]=useState();
  const token = localStorage.getItem("token");
  
  
  const changeHandler = (e:any) => {
		setCv(e.target.files[0]);
		setIsFilePicked(true);
    console.log(cv,"cv");
	};
  useEffect(() => {
    const getDetailsData = async () => {
      try {
        await axios
          .get("/students", {
            headers: {
              Authorization: token,
            },
          })
          .then((response) => {
            console.log(response);
            const detailsdata = response.data.message;
            setDetailsTableData(detailsdata);
          });
      } catch (error) {
        console.log(error);
      }
    };
    getDetailsData();
  }, []);
  console.log(detailstabledata);
 const submitForm=(e:any)=>{
  e.preventDefault();
  const payroll={
    name:name,
    year:year,
    average:average,
    cv:cv,
  }
  const formData=new FormData();
  formData.append("name",name)
  formData.append("year",year)
  formData.append("average",average)
  if(cv)
  formData.append("cv",cv)
   console.log("vykcdbs");
  const setDetailsData = async () => {
    try {
      await axios
        .post("/students/create",formData, {
          headers: {
            Authorization: token,
            'content-type': 'multipart/form-data',
          },
        })
        .then((response) => {
          console.log(response);
          //setDetailsTableData([...detailstabledata,payroll]);
          // const detailsdata = response.data.message;
          // setDetailsTableData(detailsdata);
        });
    } catch (error) {
      console.log(error);
    }
  };
  setDetailsData();
}
// const UpdateStudent=()=>{
//   const setUpdateStudent = async () => {
//     try {
//       await axios
//         .put("/students", {
//           headers: {
//             Authorization: token,
//             'content-type': 'multipart/form-data',
//           },
//         })
//         .then((response) => {
//           console.log(response);
//         });
//     } catch (error) {
//       console.log(error);
//     }
//   };
//   setUpdateStudent();
// } 
//}
 
  
  return (
    <div>
      <div className={styles.heading}>
        <h1>Add New Activity</h1>
        {/* <button className={styles.savebtn}>Save</button> */}
      </div>
      <div>
        {/* <div className={styles.data}>
          <Box
            component="form"
            sx={{
              "& > :not(style)": {m: 1, width: "25ch"},
            }}
            noValidate
            autoComplete="off"
          >
            <TextField
              id="outlined-basic"
              label="Company Name"
              variant="outlined"
            />

            <TextField id="outlined-basic" label="Date" variant="outlined" />
            <TextField id="outlined-basic" label="Domain" variant="outlined" />
            <TextField
              id="outlined-basic"
              label="NoOfVaccancies"
              variant="outlined"
            />
            <button className={styles.addbtn}>Add Student</button>
          </Box>
        </div> */}
        <div>
          <form onSubmit={submitForm}>
          <TextField id="outlined-basic" label="name" variant="outlined"  onChange={(e) => {
           setName(e.target.value);}
      }/>
            <TextField id="outlined-basic" label="year" variant="outlined" onChange={(e)=>setYear(e.target.value)}/>
            <TextField
              id="outlined-basic"
              label="average"
              variant="outlined"
              onChange={(e)=>setAverage(e.target.value)}
            />
      <label htmlFor="contained-button-file">
        <Input accept="image/*" id="contained-button-file" multiple type="file"  onChange={changeHandler}/>
        <Button className={styles.uploadbtn} variant="contained" component="span">
          Upload Cv
        </Button>  
      </label>
             {/* <TextField id="outlined-basic" label="cv" variant="outlined" onChange={(e)=>setCv(e.target.value)}/> */}
             {/* <a href="./yourfile.pdf" download> Download CV</a> */}
             <button type="submit" className={styles.addbtn}>Add Student</button>
            
          </form>
        </div>
      </div>
      <Tabledetails rows={detailstabledata} />
      {/* <Addtabletoactivity
        rows={[
          {
            id: "1",
            Student: "Shivani",
            Year: "1998",
            numberOfStudents: "10",
            Avg: "80",
            CV: "cv",
            Details: "Details",
          },
        ]}
      /> */}
    </div>
  );
};
export default Addnewactivity;

//<a href='/somefile.txt' download>Click to download</a>--->cv
