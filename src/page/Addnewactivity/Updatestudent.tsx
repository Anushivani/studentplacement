import { Button, TextField } from "@material-ui/core";
import { styled } from '@mui/material/styles';
import axios from "axios";
//import Button from '@mui/material/Button';
import { useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import styles from "./Addnewactivity.module.scss";
interface Iparam {
    companyId: string;
}
const Updatestudent = () => {
    const token = localStorage.getItem("token");
    const { companyId } = useParams<Iparam>();
    const [name, setName] = useState("");
    const [year, setYear] = useState("");
    const [average, setAverage] = useState("");
    const [cv, setCv] = useState();
    const [isFilePicked, setIsFilePicked] = useState(false);
    const [updatedstudent, setUpdatedstudent] = useState([]);
    const Input = styled('input')({
        display: 'none',
    });
    const changeHandler = (e: any) => {
        setCv(e.target.files[0]);
        setIsFilePicked(true);

    };
    const history=useHistory();
    const submitForm = (e: any) => {
        e.preventDefault();
        const payroll = {
            name: name,
            year: year,
            average: average,
            cv: cv,
        }
        const formData = new FormData(
        );
        formData.append("id", companyId)
        formData.append("name", name)
        formData.append("year", year)
        formData.append("average", average)
        if (cv)
            formData.append("cv", cv)
        console.log("vykcdbs");
        const setDetailsData = async () => {
            try {
                await axios
                    .put("/students", formData, {
                        headers: {
                            Authorization: token,
                            'content-type': 'multipart/form-data',
                        },
                    })
                    .then((response) => {
                        console.log(response);
                        //setDetailsTableData([...detailstabledata,payroll]);
                        // const detailsdata = response.data.message;
                        // setDetailsTableData(detailsdata);
                    });
            } catch (error) {
                console.log(error);
            }
        };
        setDetailsData();
        history.push("/Addnewactivity")
    }


console.log(cv,"cv");
return (
    <div>
        <form onSubmit={submitForm}>
            <TextField id="outlined-basic" label="name" variant="outlined" onChange={(e) => {
                setName(e.target.value);
            }
            } />
            <TextField id="outlined-basic" label="year" variant="outlined" onChange={(e) => setYear(e.target.value)} />
            <TextField
                id="outlined-basic"
                label="average"
                variant="outlined"
                onChange={(e) => setAverage(e.target.value)}
            />
            <label htmlFor="contained-button-file">
                <Input accept="image/*" id="contained-button-file" multiple type="file" onChange={changeHandler} />
                <Button className={styles.updatecvbtn} variant="contained" component="span">
                    Upload
                </Button>
            </label>
            {/* <TextField id="outlined-basic" label="cv" variant="outlined" onChange={(e)=>setCv(e.target.value)}/> */}
            {/* <a href="./yourfile.pdf" download> Download CV</a> */}
            <button type="submit" className={styles.addStudentbtn}>Add Student</button>

        </form>
    </div>

);
}
export default Updatestudent;
//onSubmit={submitForm}
//onChange={changeHandler}