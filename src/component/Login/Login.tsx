import styles from "./Login.module.scss";
import {useForm} from "react-hook-form";
import {yupResolver} from "@hookform/resolvers/yup";
import * as yup from "yup";
import {IForm} from "./Login.types";
import {useState} from "react";
import {Link, useHistory} from "react-router-dom";
import Home from "../../page/Home/Home";
// import Validate from "./Validate";
import axios from "axios";

const schema = yup.object().shape({
  username: yup
    .string()
    .required("Please Enter your email")
    .matches(/^[^\s@]+@[^\s@]+\.[^\s@]+$/),

  password: yup
    .string()
    .required("Please Enter your password")
    .matches(
      /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
      "Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and one special case Character"
    ),
});

const Login = () => {
  const history = useHistory();
  const [error, setError] = useState(false);
  const [token, setToken] = useState("");
  const {
    register,
    handleSubmit,
    formState: {errors},
  } = useForm<IForm>({
    resolver: yupResolver(schema),
  });

  // const handleclick = () => {
  //   console.log("formdata submited");
  // };onClick={handleclick}
  const onSubmit = async (data: IForm) => {
    await axios
      .post("/login", {
        username: data.username,
        password: data.password,
      })
      .then((response) => {
        console.log(response);
        if (response.data.token) {
          alert("login failed");
        } else {
          //setToken(response.data.token);
          const token = localStorage.setItem(
            "token",
            response.data.message.token
          );
          alert("login successfull");
          history.push("/Home");
          //history.push("/Home#`${data.id}`");
        }
      });
  };
  console.log(token);

  return (
    <>
      <div className={styles.container}>
        <div className={styles.maincontent}>
          <form onSubmit={handleSubmit(onSubmit)}>
            <h2>Login Here</h2>
            <input
              type="email"
              placeholder="email"
              className={styles.textFiled}
              {...register("username")}
            />
            <p> {errors.username?.message}</p>
            <input
              type="password"
              placeholder="password"
              className={styles.textFiled}
              {...register("password")}
            />
            {errors.password && <p>{errors.password.message}</p>}
            <div>
              <button className={styles.btn}>Login</button>
            </div>

            {/* <Link to="/Home">Forgot Password?</Link> */}
          </form>
        </div>
      </div>
    </>
  );
};

export default Login;