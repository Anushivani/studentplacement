export interface Props {
  rows: Array<{
    id: string;
    firstName: string;
    lastName: string;
    email: string;
  }>;
}
