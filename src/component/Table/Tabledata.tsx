import React from "react";
import PropTypes from "prop-types";
//import {withStyles} from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import {useHistory} from "react-router-dom";
//import {Props} from "./Tabledata.types";
import Addnewactivity from "../../page/Addnewactivity/Addnewactivity";
import styles from "./Tabledata.module.scss";

interface Props {
  rows: Array<{
    id: number;
    company: string;
    domain: string;
    vacancy: number;
    Action: string;
  }>;
}

const Tabledata = ({rows}: Props) => {
  const history = useHistory();
  const addNewActivity = () => {
    history.push("/Addnewactivity");
  };
  const ActivityDetails = () => {
    history.push("/Details");
  };
  return (
    <div>
      {/* <div>
        <h1>Today's Campus Activity</h1>
        <button className={styles.newbtn} onClick={addNewActivity}>
          Add New
        </button>
      </div> */}
      <Paper>
        <Table>
          <TableHead className={styles.heading}>
            <TableRow>
              <TableCell>Id</TableCell>
              <TableCell>Company</TableCell>
              <TableCell>Domain</TableCell>
              <TableCell>numberOfStudents</TableCell>
              <TableCell>Action</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row: any) => (
              <TableRow key={row.id}>
                <TableCell>{row.id}</TableCell>
                <TableCell>{row.company}</TableCell>
                <TableCell>{row.domain}</TableCell>
                <TableCell>{row.vacancy}</TableCell>
                <TableCell>
                  <button className={styles.edit} onClick={addNewActivity}>
                    edit
                  </button>
                  <button className={styles.Details} onClick={ActivityDetails}>
                    Details
                  </button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Paper>
    </div>
  );
};
export default Tabledata;
// import React from "react";
// import PropTypes from "prop-types";
// //import {withStyles} from "@material-ui/core/styles";
// import Table from "@material-ui/core/Table";
// import TableBody from "@material-ui/core/TableBody";
// import TableCell from "@material-ui/core/TableCell";
// import TableHead from "@material-ui/core/TableHead";
// import TableRow from "@material-ui/core/TableRow";
// import Paper from "@material-ui/core/Paper";
// //import {Props} from "./Tabledata.types";

// interface Props {
//   rows: Array<{
//     id: string;
//     firstName: string;
//     lastName: string;
//     email: string;
//   }>;
// }
// const Tabledata = ({rows}: Props) => {
//   return (
//     <Paper>
//       <Table>
//         <TableHead>
//           <TableRow>
//             <TableCell>Id</TableCell>
//             <TableCell>First Name</TableCell>
//             <TableCell>Last Name</TableCell>
//             <TableCell>Email</TableCell>
//           </TableRow>
//         </TableHead>
//         <TableBody>
//           {rows.map((row) => (
//             <TableRow key={row.id}>
//               <TableCell>{row.id}</TableCell>
//               <TableCell>{row.firstName}</TableCell>
//               <TableCell>{row.lastName}</TableCell>
//               <TableCell>{row.email}</TableCell>
//             </TableRow>
//           ))}
//         </TableBody>
//       </Table>
//     </Paper>
//   );
// };
// export default Tabledata;
