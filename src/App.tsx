import Login from "./component/Login/Login";
import Home from "./page/Home/Home";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Addnewactivity from "./page/Addnewactivity/Addnewactivity";
import Details from "./page/Details/Details";
import Updatestudent from "./page/Addnewactivity/Updatestudent";

const App = () => {
  return (
    <div>
      <Router>
        <Switch>
          <Route path="/" exact component={Login} />
          <Route path="/Home" exact component={Home} />
          <Route path="/Addnewactivity" exact component={Addnewactivity} />
          <Route path="/Details/:companyId" exact component={Details} />
          {/* <Route path="/Details:companyId" exact component={Details} /> */}
          <Route path="/Updatestudent/:companyId" exact component={Updatestudent}/>
        </Switch>
      </Router>
    </div>
  );
};
export default App;
